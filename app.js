const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');

const app = express();

const uri = "mongodb://Cristopher:Cescorci10@graphql-db-shard-00-00-jhufx.mongodb.net:27017,graphql-db-shard-00-01-jhufx.mongodb.net:27017,graphql-db-shard-00-02-jhufx.mongodb.net:27017/test?ssl=true&replicaSet=GraphQL-DB-shard-0&authSource=admin&retryWrites=true&w=majority";

mongoose.connect(uri);
mongoose.connection.once('open', () => {
    console.log('connected to database');
});

// bind express with graphql
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(4000, () => {
    console.log('now listening for requests on port 4000');
});
